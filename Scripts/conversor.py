#Import das bibliotecas
import sys
import traceback
import os

#Função de Conversão para nexus
def conv_nexus():
    
    try:
        sys.argv[1]
    except IndexError:
        print('Have to chose a file.', file = sys.stderr)
        error = open('error.txt', 'w')
        traceback.print_exec(file = error)
        return(0)
    
    file = open(sys.argv[1], 'r')
    seq = {}
    
    for line in file:
        line = line.strip()
        try:
            if line.startswith('>'):
                header = line.replace('>', '')
                header = header.replace(' ', '_')
                header = header.replace('(', '_')
                header = header.replace(')', '_')
                header = header.replace(',', '_')
                header = header.replace(':', '_')
                header = header[:99]
                seq[header] = ''
            else:
                seq[header] += line
        except Exception:
            error = open('error.txt', 'w')
            traceback.print_exec(file = error)
            continue
        
    return(seq)   

#Função para os prints iniciais do nexfile e dos parametros do mrbayes       
def prints():
    
    seqs = conv_nexus()
    count = 0
    leng = 0
    nseqs = len(seqs)
    basename = os.path.basename(sys.argv[1])
    basename = basename[:-6]
    
    while count <= 1:
        for i in seqs:
            leng = len(seqs[i])
            count += 1;
            
    if len(seqs) == 0:
        print('Empty file')
        return(0)
    
    print('#NEXUS')
    print('begin Data;')
    print('dimensions ntax={} nchar={};'.format(nseqs, leng))
    print('format datatype=dna missing=N gap=-;')
    print('matrix')
    
    for seq in sorted(seqs):
        print('{}\t{}'. format(seq, seqs[seq]))
        
    print(';')
    print('end;\n')
    print('begin mrbayes;')
    print('\tset autoclose = yes nowarn=yes;')
    print('\tmcmc ngen = 1000000 nruns = 2 printfreq = 1000 samplefreq = 100 diagnfreq = 1000 nchains = 4 filename = {};'.format(basename))
    print('\tsumt filename = {};'.format(basename))
    print('\tquit;')
    print('end;')
    
    return()

seqs = {}
prints()
