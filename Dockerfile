#
#Dockerfile
#

#Image used for our docker
FROM snakemake/snakemake:main

#The maintainer of this dockerfile
LABEL MAINTAINER = 201900200@estudantes.ips.pt

#Directorys creation
WORKDIR dockTree
COPY Scripts/ /dockTree/Scripts/
COPY input/ /dockTree/input/
COPY output/ /dockTree/output/
COPY Snakefile /dockTree/

#Install all needings and dependencies
RUN apt-get update && \
	apt-get install apt-utils -y &&\
	apt-get install raxml -y && \
	apt-get install mrbayes -y && \
	apt-get install mafft -y && \
 	apt-get install clustalo -y 

#Instal miniconda
RUN conda config --add channels defaults && \
	conda config --add channels bioconda && \
	conda config --add channels conda-forge

#instal mamba to install R
RUN mamba install r-base -y
	
#Install all R packages used	 
RUN Rscript -e "install.packages('ape', repos='https://cran.radicaldevelop.com/')" && \
	Rscript -e "install.packages('phangorn', repos='https://cran.radicaldevelop.com/')" && \
	Rscript -e "install.packages('BiocManager', repos='https://cran.radicaldevelop.com/')" && \
	Rscript -e "BiocManager::install('treeio')"


