#Importe das bibliotecas
import os
import glob
import re

#Criação das variáveis da command line
inputvar= os.environ.get("INPUT")
outputvar= os.environ.get("OUTPUTVAR")

#command: OUTPUTVAR = "outputs" INPUT = "Test/AK_tbr1.fasta" snakemake --cores all

singularity: "docker://diogomaria/teamd:latest"

#Rule que inicializa os diretorios
rule todas:
	input:
		f'{outputvar}/fasta/aln_mafft.fasta',
		f'{outputvar}/fasta/aln_clustalo.fasta',
		f'{outputvar}/nexus/aln_mafft.nex',
		f'{outputvar}/nexus/aln_clustalo.nex',
		f'{outputvar}/raxml/RAxML_bipartitionsBranchLabels.raxml_mafft',
		f'{outputvar}/raxml/RAxML_bipartitionsBranchLabels.raxml_clustalo',
		f'{outputvar}/mrbayes/aln_mafft.con.tre',
		f'{outputvar}/mrbayes/aln_clustalo.con.tre',
		f'{outputvar}/results/mafft_results.txt',
		f'{outputvar}/results/clustalo_results.txt'
	message:
		'Work :)'

rule limpar:
	shell:
		'cd {outputvar}/ && rm -rf fasta nexus raxml mrbayes txt results'

#Alinha as sequências Fasta com o método mafft
rule mafft:
	input:
		inputvar
	output:
		'{outputvar}/fasta/aln_mafft.fasta'
	run:
		shell(f'mkdir -p {outputvar}/fasta')
		shell('mafft --auto {input} > {output}')

#Alinha as sequências Fasta com o método Clustal Omega
rule clustalo:
	input:
		inputvar
	output:
		'{outputvar}/fasta/aln_clustalo.fasta'
	run:
		shell(f'mkdir -p {outputvar}/fasta')
		shell('clustalo --in={input} > {output}')

#Converte ficheiros de Fasta para Nexus usando um script de python3
rule conversor:
	input:
		'{outputvar}/fasta/aln_mafft.fasta',
		'{outputvar}/fasta/aln_clustalo.fasta'
	output:
		'{outputvar}/nexus/aln_mafft.nex',
		'{outputvar}/nexus/aln_clustalo.nex'
	run:
		shell(f'mkdir -p {outputvar}/nexus')
		shell('python3 Scripts/conversor.py {outputvar}/fasta/aln_mafft.fasta > {outputvar}/nexus/aln_mafft.nex')
		shell('python3 Scripts/conversor.py {outputvar}/fasta/aln_clustalo.fasta > {outputvar}/nexus/aln_clustalo.nex')

#Cria as árvores filogenéticas usando o programa RAxMl para o ficheiro Fasta
rule raxml:
	input:
		'{outputvar}/fasta/aln_mafft.fasta',
		'{outputvar}/fasta/aln_clustalo.fasta'
	output:
		'{outputvar}/raxml/RAxML_bipartitionsBranchLabels.raxml_mafft',
		'{outputvar}/raxml/RAxML_bipartitionsBranchLabels.raxml_clustalo'
	run:
		shell(f'mkdir -p {outputvar}/raxml')
		shell('raxmlHPC-PTHREADS-SSE3 -f a -m GTRCAT -p 112358 -x 112358 -N 100 -s {outputvar}/fasta/aln_mafft.fasta -n raxml_mafft')
		shell('raxmlHPC-PTHREADS-SSE3 -f a -m GTRCAT -p 112358 -x 112358 -N 100 -s {outputvar}/fasta/aln_clustalo.fasta -n raxml_clustalo')
		shell('mv RAxML* {outputvar}/raxml/')

#Cria as árvores filogenéticas usando o programa MrBayes para o ficheiro Nexus convertido em python3
rule mrbayes:
	input:
		'{outputvar}/nexus/aln_mafft.nex',
		'{outputvar}/nexus/aln_clustalo.nex'
	output:
		'{outputvar}/mrbayes/aln_mafft.con.tre',
		'{outputvar}/mrbayes/aln_clustalo.con.tre'
	run:
		shell(f'mkdir -p {outputvar}/mrbayes')
		shell('mb {outputvar}/nexus/aln_mafft.nex')
		shell('mb {outputvar}/nexus/aln_clustalo.nex')
		shell('mv aln* {outputvar}/mrbayes/')

#Comparasão das árvores filogenéticas feitas anteriormente usando um script de R conhecido como Shimodaira Hasegawa
rule comparision_R:
	input:
		'{outputvar}/nexus/aln_mafft.nex',
		'{outputvar}/fasta/aln_mafft.fasta',
		'{outputvar}/mrbayes/aln_mafft.con.tre',
		'{outputvar}/raxml/RAxML_bipartitionsBranchLabels.raxml_mafft',
		'{outputvar}/nexus/aln_clustalo.nex',
		'{outputvar}/fasta/aln_clustalo.fasta',
		'{outputvar}/mrbayes/aln_clustalo.con.tre',
		'{outputvar}/raxml/RAxML_bipartitionsBranchLabels.raxml_clustalo'
	output:
		'{outputvar}/results/mafft_results.txt',
		'{outputvar}/results/clustalo_results.txt'
	run:
		shell(f'mkdir -p {outputvar}/results')
		shell('Rscript Scripts/Shimodaira_Hasegawa_final.R {outputvar}/nexus/aln_mafft.nex {outputvar}/fasta/aln_mafft.fasta {outputvar}/mrbayes/aln_mafft.con.tre {outputvar}/raxml/RAxML_bipartitionsBranchLabels.raxml_mafft')
		shell('mv Tree_Comparison.txt {outputvar}/results/mafft_results.txt')
		shell('Rscript Scripts/Shimodaira_Hasegawa_final.R {outputvar}/nexus/aln_clustalo.nex {outputvar}/fasta/aln_clustalo.fasta {outputvar}/mrbayes/aln_clustalo.con.tre {outputvar}/raxml/RAxML_bipartitionsBranchLabels.raxml_clustalo')
		shell('mv Tree_Comparison.txt {outputvar}/results/clustalo_results.txt')
