# Comparação entre Diferentes Árvores Filogenéticas 

Este Projeto tem como objetivo a construção de uma pipeline de comparação de árvores filogenéticas, ou seja, construir um programa que terá como output a comparação de diversas árvores filogenéticas. Esta pipeline encarrega-se de alinhar as sequências, construir as árvores filogenéticas e por fim, compará-las. O utilizador terá de introduzir as sequências biológicas que pretende analisar e a partir desses dados irá obter uma comparação da evolução filogenética.




# Métodos

1. Introduzir um ficheiro, do tipo Fasta, como input.
2. Alinhar a sequência, usando o MAFFT e o Clustalo.
3. Conversão de Fasta para Nexus.
4. Criação de Árvores, usando o RAxML em Fasta e o MrBayes em Nexus.
5. Comparação das Árvores Filogenéticas feita em R usando o método de Shimodaira Hasegawa.




# Pipeline para a Comparação das Árvores Filogenéticas

O utilizador terá que ter o docker instalado após isso tem que usar os seguintes comandos:
1) Pull Docker image: docker pull diogomaria/teamd:lastest
2) Correr o Docker : docker run -v $HOME/Desktop/path_to_file_in/input/:/dockTree/input -v $HOME/Desktop/path_to_file_out/output/:/dockTree/output -it diogomaria/teamd:latest bin/bash/
* Nota: No passo 2 o utilizador tem de mudar o PATH (path_to_file_in) de onde se localizam os ficheiros de input como também o PATH (path_to_file_out) para os ficheiros de output.
3) Ir para o diretorio: cd /dockTree/
* Nota: O passo 3 serve para garantir que o utilizar está no diretorio correto.
4) Correr o SnakeFile : OUTPUTVAR='output' INPUT='input_file' snakemake --cores all
* Nota: No passo 4 o "input_file" é a sequência que o utilizador pretende analisar.




# Requesitos

* Instalação do Docker
